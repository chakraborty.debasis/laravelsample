<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/* Auth user routes */
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    /* Profile pages routes */
    Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);
    Route::post('/save_profile', ['as' => 'save_profile', 'uses' => 'ProfileController@save_profile']);

    /* Users */
    Route::get('/users', ['as' => 'users', 'uses' => 'UserController@index']);
    Route::post('/users', ['as' => 'users', 'uses' => 'UserController@index']);
});

/* Administrative user routes */
Route::group(['middleware' => 'admin'], function () {
    Route::post('/save_user', ['as' => 'save_user', 'uses' => 'UserController@save_user']);
    Route::post('/check_user', ['as' => 'check_user', 'uses' => 'UserController@check_user']);
    Route::get('/user_delete/{user_id}', ['as' => 'user_delete', 'uses' => 'UserController@user_delete']);
});
