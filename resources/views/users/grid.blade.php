@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
    <div class="modal fade" id="userModal" role="dialog">
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!-- <h4 class="modal-title user_header">Modal Header</h4> -->
    </div>
    <div class="modal-body">
        <form method="POST" action="{{url('save_user')}}" id="user_form" name="user_add_edit">
        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="id" value="{{ $data['id'] }}">
        <div class='row'>
            <div class='col-md-6'>
                <div class="form-group">

            {!! Form::label('User Type') !!}
            {!! Form::select('user_level', $data['user_levels'], null, array('id'=>'user_level','class'=>'form-control', 'required' => 'required')) !!}
            </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-6'>
                <div class="form-group">

            {!! Form::label('Name') !!}
            {!! Form::text('name', $data['name'], array('id'=>'name','class'=>'form-control','required' => 'required')); !!}
            <p id="name_valid" style="margin-left: 40px; color: red;"><sub>Name is required</sub></p>
            </div>
            </div>
            <div class='col-md-6'>
            <div class="form-group">
            {!! Form::label('Email') !!}
            {!! Form::text('email', $data['email'], array('id'=>'email','class'=>'form-control','size' => 100, 'required' => 'required')); !!}
            <p style="margin-left: 40px;"><sub>Email Address is also Username</sub></p>
            <p  id="email_valid" style="margin-left: 40px; color: red;"><sub>Email Id is invalide or empty</sub></p>
            </div>
            </div>
        </div>
        <div class='row'>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Password</label>


                    <input id="password" type="password" class="form-control" name="password" value="" <?php echo (!isset($data['id'])) ? 'required="required"' : '' ?>>
                    <p  id="password_valid" style="margin-left: 40px; color: red;"><sub>Password is required</sub></p>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>
        </div>


        <div class="col-md-6">
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

            {!! Form::label('Confirm Password') !!}

                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" value="" <?php echo (!isset($data['id'])) ? 'required="required"' : '' ?>>

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
                <span class="help-block pass" style="color:red">
                    <strong>Password and password confirmation must match!</strong>
                </span>
        </div>
        </div>
        </div>
        <div class="form-group email_error">
            <p class="alert alert-danger email_msg" style="text-align: center;margin-top: 20px;">
            
            </p> 
        </div> 
    </div>

    <div style="clear: both; padding-top: 25px;"></div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" id="user_save_btn" class="btn btn-primary">Save</button>
        </form>
    </div>
    </div>

</div>
</div>    
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">User List
                @if(\Auth::user()->isAdmin())
                <a href="#" style="float: right;" id="add_user" class="btn btn-primary">Add new user</a>
                @endif
                </div>

                <div class="card-body">
                    @if (session('success_message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p>{{{ session('success_message') }}}</p>
                        </div>
                    @endif

                    @if (session('error_message'))
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p>{{{ session('error_message') }}}</p>
                        </div>
                    @endif
                        <div class="dataTables_length" id="agent-table_length">
                            <form method="POST" action="{{route('users')}}" role="sort">
                            {{ csrf_field() }}
                            <label>Show
                            <select name="paginate_show" aria-controls="agent-table" class="" onchange="this.form.submit()">
                            <option value="5" <?php if ($paginate_show == 5): ?>
                                            <?php echo "selected" ?>
                                        <?php endif?>>5
                            </option>
                            <option value="10" <?php if ($paginate_show == 10): ?>
                                            <?php echo "selected" ?>
                                        <?php endif?>>10
                            </option>
                            <option value="25" <?php if ($paginate_show == 25): ?>
                                            <?php echo "selected" ?>
                                        <?php endif?>>25
                            </option>
                            <option value="50" <?php if ($paginate_show == 50): ?>
                                            <?php echo "selected" ?>
                                        <?php endif?>>50
                            </option>
                            <option value="100" <?php if ($paginate_show == 100): ?>
                                            <?php echo "selected" ?>
                                        <?php endif?>>100
                            </option>
                            </select> entries</label>
                            </form>
                        </div>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Level</th>
                                <th>Email</th>
                                @if(\Auth::user()->isAdmin())
                                <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->level }}</td>
                                <td>{{ $user->email }}</td>
                                @if(\Auth::user()->isAdmin())
                                <td>
                                   <a style="float: left;" href="#" user_id="{{$user->id}}" name="{{$user->name}}" email="{{$user->email}}" user_level="{{$user->user_level}}" class="btn btn-primary user_edit">Edit</a>
                                   <a style="margin-left: 6px;" onclick="return confirm('Are you certain that you wish to delete this user?')" href="{{ url('/user_delete/' . $user->id) }}" user_id="{{$user->id}}" class="btn btn-danger user_delete">Delete</a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            </tbody>
                            
                        </table>   
                        {{ $users->appends(array('paginate_show'=>$paginate_show))->links() }}         
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function(){
     $('.pass').hide();
     $('#name_valid').hide();
     $('#email_valid').hide();
     $('#password_valid').hide();
     $('.email_error').hide();

    $('#user_save_btn').click(function(){
        var name = $('#name').val();
        var email = $('#email').val();
        var user_id = $('#user_id').val();
        var password = $('#password').val();


        if (name=='') {
           $('#name_valid').show();
           return false;
        }else{
           $('#name_valid').hide();
        }

        if (!validateEmail(email) || email=='') {
           $('#email_valid').show();
           return false;
        }else{
           $('#email_valid').hide();
        }

        if (user_id=='') {
            if (password=='') {
               $('#password_valid').show();
               return false;
            }else{
               $('#password_valid').hide();
            }
        }
        $('form#user_form').submit();
    });

    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
    }
    $("#password_confirmation").change(function(){
        if ($('#password').val() && $('#password_confirmation').val()) {
            if ($('#password_confirmation').val() == $('#password').val()) {
               $('#user_save_btn').prop('disabled', false);
               $('.pass').hide();
               $('#password_valid').hide();
            }else{
                $('#user_save_btn').prop('disabled', true);
                $('.pass').show();
            }
        }else{
            if (!$('#password').val() && !$('#password_confirmation').val()) {
                $('#user_save_btn').prop('disabled', false);
                $('.pass').hide();
                $('#password_valid').hide();
            }else{
                $('#user_save_btn').prop('disabled', true);
                $('.pass').show();
            }
        }
    });
    $("#password").change(function(){
        if ($('#password').val() && $('#password_confirmation').val()) {
            if ($('#password_confirmation').val() == $('#password').val()) {
               $('#user_save_btn').prop('disabled', false);
               $('.pass').hide();
               $('#password_valid').hide();
            }else{
                $('#user_save_btn').prop('disabled', true);
                $('.pass').show();
            }
        }else{
            if (!$('#password').val() && !$('#password_confirmation').val()) {
                $('#user_save_btn').prop('disabled', false);
                $('.pass').hide();
                $('#password_valid').hide();
            }else{
                $('#user_save_btn').prop('disabled', true);
                $('.pass').show();
            }

        }
    });
    $(".user_edit").click(function(){
     $('.pass').hide();
     $('#name_valid').hide();
     $('#email_valid').hide();
     $('#password_valid').hide();

        $('.user_header').html("Edit User");

        var user_id = $(this).attr('user_id');
        var name = $(this).attr('name');
        var email = $(this).attr('email');
        var user_level = $(this).attr('user_level');

        $('#user_form')[0].reset();

        $('#name').val(name);
        $('#user_id').val(user_id);
        $('#email').val(email);
        $('#user_level').val(user_level);

        $('#userModal').modal('show');
    });
    $("#add_user").click(function(){
        $('#user_form')[0].reset();
        $('#user_id').val('');
        $('.user_header').html("Add new user");
        $('#userModal').modal('show');
    })
    $('#user_form').on('submit', function(e){
        $('.email_error').hide();
        var self = this;
        e.preventDefault();
        var email = $('#email').val();
        var user_id = $('#user_id').val();
        if(email){
            $_token = "{{ csrf_token() }}";

            $.ajax({
                type: "POST",
                url: "{{ url('/check_user') }}",
                data: {email: email, _token: $_token, user_id: user_id},
                success: function( response ) {
                    if(response.status == 'user_found'){
                        $('.email_error').show();
                        $('.email_msg').html('This email address is already in our system. <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>');
                    }else{
                        var password = $('#password').val();
                        if(user_id =='' && password ==''){
                           $('.email_error').show();
                           $('.email_msg').html('Password can not be blank for new user. <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>');
                        }
                        else{
                         self.submit();
                        }
                    }
                }
            });
        }
    });     
});
</script>
@endpush
