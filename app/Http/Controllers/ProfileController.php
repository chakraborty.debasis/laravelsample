<?php

namespace App\Http\Controllers;

/*Load required models*/
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfile;

class ProfileController extends Controller
{
    public function __construct()
    {
        // force authentication for this page
        $this->middleware('auth');
    }

    /*Profile index page*/
    public function index()
    {
        //Get current logedin user details
        $data['user'] = User::find(Auth::user()->id)->toArray();

        //Redirect to profile index page
        return view('profile.index', $data);
    }

    /*Profile Save*/
    public function save_profile(UpdateProfile $request)
    {
        $data = $request->input();
        $user = User::find(Auth::user()->id);

        if ($data['password'] != $data['password_confirmation']) {
            return redirect()->route('profile')->with('error_message', 'Password and password confirmation must match!');
        } else {
            if ($data['password'] != null && $data['password'] == $data['password_confirmation']) {
                $ret = $user->forceFill([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'remember_token' => md5(microtime()),
                ])->save();
            } else {
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->save();
            }
            return redirect()->route('profile')->with('success_message', 'Profile Updated Successfully');
        }
    }
}
