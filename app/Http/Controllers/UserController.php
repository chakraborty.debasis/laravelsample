<?php

namespace App\Http\Controllers;

use App;
use App\Http\Requests\UpdateProfile;
use App\User;
use App\User_Level;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        //Pagination show entry
        if (isset($request->paginate_show)) {
            $paginate_show = $request->paginate_show; //Set dynamic value
        } else {
            $paginate_show = 10; //Set Default value
        }

        $users = User::paginate($paginate_show);

        $data = array('id' => '', 'name' => '', 'email' => '', 'user_level' => 2);

        $data['user_levels'] = User_Level::orderBy('user_level')->pluck('user_level', 'id')->toArray();

        $data['current_user_id'] = Auth::user()->id;

        //Return View
        return view('users.grid', compact('users'))
            ->with('paginate_show', $paginate_show)
            ->with('data', $data);
    }

    public function save_user(UpdateProfile $request)
    {
        $data = $request->input();

        if ($data['id']) {
            $user = User::find($data['id']);
        } else {
            $user = new User;
        }

        if ($data['password'] != $data['password_confirmation']) {
            return redirect()->route('users')->with('error_message', 'Password and password confirmation must match!');
        } else {
            if ($data['password'] != null && $data['password'] == $data['password_confirmation']) {
                $ret = $user->forceFill([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'user_level' => $data['user_level'],
                    'password' => bcrypt($data['password']),
                    'remember_token' => md5(microtime()),
                ])->save();
            } else {
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->user_level = $data['user_level'];
                $user->save();
            }
            if ($data['id']) {
                return redirect()->route('users')->with('success_message', 'User Updated Successfully');
            } else {
                return redirect()->route('users')->with('success_message', 'User Created Successfully');
            }
        }
    }

    public function check_user(Request $request)
    {
        $email = $request->email;
        $user_id = $request->user_id;

        if ($user_id) {
            //Update
            $user = User::where('id', '!=', $user_id)->where('email', $email)->first();
        } else {
            //Create
            $user = User::where('email', $email)->first();
        }

        if ($user) {
            $response = array(
                'status' => 'user_found',
            );
        } else {
            $response = array(
                'status' => 'user_not_found',
            );
        }

        return \Response::json($response);
    }

    public function user_delete($id)
    {
        $getUser = User::find($id);
        if ($getUser->count() > 0) {
            $user_email = $getUser->email;
            $getUser->delete();
            return redirect()->route('users')->with('success_message', 'User Deleted Successfully');
        } else {
            return redirect()->route('users')->with('error_message', 'Invalide User! Please try again');
        }
    }
}
