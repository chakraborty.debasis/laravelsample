<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request as UserInput;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(UserInput $request)
    {
        $rules= [
            'name' => 'required|max:255',
        ];
        
        if (isset($request->id) && isset($request->id)!="") {
            $rules['email'] = 'required|email|max:255|unique:users,email,'.$this->id;
        }

        if (isset($request->password) && isset($request->password)!=null) {
            $rules['password']  = 'required|string|min:6|confirmed';
        }

        return $rules;
    }
}
