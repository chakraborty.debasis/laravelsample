<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_level',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'level',
    ]; 

    public function level()
    {
        return $this->hasOne('App\User_Level', 'id', 'user_level');
    }

    public function getLevelAttribute()
    {
        return $this->level()->first()->user_level;
    }

    public function isAdmin()
    {
        return $this->user_level == 1;
    }

    public function isUser()
    {
        return $this->user_level == 2;
    }
}
