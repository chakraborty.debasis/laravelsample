-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for php_laravel
DROP DATABASE IF EXISTS `php_laravel`;
CREATE DATABASE IF NOT EXISTS `php_laravel` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `php_laravel`;

-- Dumping structure for table php_laravel.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table php_laravel.migrations: 4 rows
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_05_18_070856_add_user_levels_table', 2),
	(4, '2018_05_18_071450_update_users_table_1', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table php_laravel.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table php_laravel.password_resets: 0 rows
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table php_laravel.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_user_level_foreign` (`user_level`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table php_laravel.users: 0 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `user_level`) VALUES
	(1, 'Laravel Admin', 'admin@gmail.com', '$2y$10$D7Gg1wgdkrHGBwLX.HF95eusijPlJR2kMLu4jp3yDlktBC0oAukPS', 'MjfwwblHsOxwe16nzqqC63ZGwsi32ryUU5AWS5OKH2aaWLDhQmKFJ5orQ1iJ', NULL, '2018-05-18 05:02:10', 1),
	(2, 'Laravel User', 'user@gmail.com', '$2y$10$vih4VbqfMMV/5N5BmSGlpuFHRNpH6KvL7/fZpYDcWdo50gt0y0p9y', 'qjQKboba7o3ZaXh9w7lRBZib1uL2mh6sI6F3Fo8lXgvHAeCZFDyBIG5vdgDM', '2018-05-18 01:55:54', '2018-05-18 01:55:54', 2),
	(4, 'Test 1', 'test1@gmail.com', '$2y$10$ufPy2NHytdrFbBmdPFoWe.ZNGf/hdMAuE4/thsBVm/BaLb3awX73C', '9612d36d2bc414fea9b68ba4e98ba7b6', '2018-05-18 05:21:00', '2018-05-18 05:21:00', 1),
	(5, 'Test By Deb', 'bfox@comcast.net', '$2y$10$bm16tY017hbL0r0yj0Jgku/uyOTfKXLVE0zG6HXw8HrbSEE9yW56i', '3aa1c0c20c59dd6ab8086ac633b30b15', '2018-05-18 05:26:30', '2018-05-18 05:26:30', 1),
	(6, 'Laravel Admin 1', 'admin1@gmail.com', '$2y$10$/L0sbD3JKYq90G67/CzDaeESSWErAGkYSCmHSslPTa/bbaEKh.7.C', '1a88ba7f8104a736bc65250ae66242f7', '2018-05-18 05:31:21', '2018-05-18 05:31:21', 1),
	(7, 'Laravel Admin 2', 'admin2@gmail.com', '$2y$10$XPMOunhtJrTnJssAvm9kXemCjoBZTDSsyuWuAFJUX.7h9eteiFho.', '8a0e2a4dcb889e309e389d7547c93399', '2018-05-18 05:42:02', '2018-05-18 05:42:02', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table php_laravel.user_levels
DROP TABLE IF EXISTS `user_levels`;
CREATE TABLE IF NOT EXISTS `user_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table php_laravel.user_levels: 2 rows
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;
INSERT INTO `user_levels` (`id`, `user_level`, `created_at`, `updated_at`) VALUES
	(1, 'Administrator', NULL, NULL),
	(2, 'User', NULL, NULL);
/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
