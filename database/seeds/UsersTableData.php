<?php

use Illuminate\Database\Seeder;

class UsersTableData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Laravel Admin",
            'user_level' => 1,
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
