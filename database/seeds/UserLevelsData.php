<?php

use Illuminate\Database\Seeder;

class UserLevelsData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_levels')->insert([
            'user_level' => 'Administrator',
        ]);
        
        DB::table('user_levels')->insert([
            'user_level' => 'User',
        ]);
    }
}
